/* window.js
 *
 * Copyright 2019 Bastian Ilsø
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Gio, GLib, GObject, Gtk, GdkPixbuf } = imports.gi;

var PodiumWindow = GObject.registerClass({
    GTypeName: 'PodiumWindow',
    Template: 'resource:///com/bastianilso/Podium/window.ui',
    InternalChildren: ['label', 'folderChooseButton', 'stack', 'carousselImage', 'nextButton']
}, class PodiumWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ application });
        this._folderChooseButton.connect('file-set', () => {
          this._onFolderChosen();
        });
        this._nextButton.connect('clicked', () => {
          this._readNextFile();
        });
    }

    _onFolderChosen() {
      let folder = this._folderChooseButton.get_file();
      this._stack.visible_child_name = 'caroussel';

      let files = folder.enumerate_children_async(
        'standard::name',
        Gio.FileQueryInfoFlags.NONE,
        GLib.PRIORITY_DEFAULT,
        null,
        (source, result, data) => {
          this._fileEnumerator = null;
          try {
            this._fileEnumerator = folder.enumerate_children_finish(result);
          } catch (e) {
            log('(Error) Could not retreive list of files! Error:' + e);
            return;
          }
          this._nextButton.sensitive = true;
          this._readNextFile();
        });
    }

    _readNextFile() {
      if (!this._fileEnumerator)
        return;

      let fileInfo = null;
      this._fileEnumerator.next_files_async(
        1,
        GLib.PRIORITY_DEFAULT,
        null,
        (source, result, data) => {
          try {
            fileInfo = this._fileEnumerator.next_files_finish(result);
          } catch (e) {
            log('Could not retreive the next file! Error:' + e);
            return;
          }
          let file = fileInfo[0].get_name();
          let filePath = GLib.build_filenamev([this._folderChooseButton.get_filename(), file]);
          let [width, height] = this.get_size();
          let pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filePath,width,height,true);
          //pixbuf = pixbuf.scale_simple(width, height, Gdk.INTERP_BILINEAR);
          this._carousselImage.set_from_pixbuf(pixbuf);
      });
    }
});

